/* dsa.c
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 1998, 2010 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#include <nettle/bignum.h>
#include <nettle/dsa.h>
#include <nettle/sexp.h>
#include <nettle/sha.h>

#include "crypto.h"

#include "atoms.h"
#include "format.h"
#include "lsh_string.h"
#include "parse.h"
#include "randomness.h"
#include "sexp.h"
#include "ssh.h"
#include "werror.h"
#include "xalloc.h"

#include "dsa.c.x" 

/* DSA signatures */

/* GABA:
   (class
     (name dsa_verifier)
     (super verifier)
     (vars
       (params indirect-special "struct dsa_params"
            #f dsa_params_clear)
       (key bignum)))
*/

/* GABA:
   (class
     (name dsa_signer)
     (super signer)
     (vars
       (params indirect-special "struct dsa_params"
            #f dsa_params_clear)
       (key bignum)))
*/

static int
do_dsa_verify(struct verifier *c, enum lsh_atom algorithm,
	      uint32_t length,
	      const uint8_t *msg,
	      uint32_t signature_length,
	      const uint8_t *signature_data)
{
  CAST(dsa_verifier, self, c);
  struct sha1_ctx hash;
  uint8_t digest[SHA1_DIGEST_SIZE];

  struct simple_buffer buffer;
  uint32_t buf_length;
  const uint8_t *buf;
  enum lsh_atom atom;

  int res = 0;

  struct dsa_signature sv;

  trace("do_dsa_verify: Verifying %a signature\n", algorithm);

  if (algorithm != ATOM_SSH_DSS)
    fatal("do_dsa_verify: Internal error!\n");

  simple_buffer_init(&buffer, signature_length, signature_data);
  if (!(parse_atom(&buffer, &atom)
	&& (atom == ATOM_SSH_DSS)
	&& parse_string(&buffer, &buf_length, &buf)
	&& buf_length == 2 * DSA_SHA1_Q_OCTETS
	&& parse_eod(&buffer)))
    return 0;

  dsa_signature_init(&sv);

  nettle_mpz_set_str_256_u(sv.r, DSA_SHA1_Q_OCTETS, buf);
  nettle_mpz_set_str_256_u(sv.s, DSA_SHA1_Q_OCTETS,
			   buf + DSA_SHA1_Q_OCTETS);

  sha1_init(&hash);
  sha1_update(&hash, length, msg);
  sha1_digest(&hash, sizeof(digest), digest);

  res = dsa_verify(&self->params, self->key, sizeof(digest), digest, &sv);
  dsa_signature_clear(&sv);

  return res;
}


struct lsh_string *
format_dsa_public_key(const struct dsa_params *params, const mpz_t pub)
{
  return ssh_format("%a%n%n%n%n",
		    ATOM_SSH_DSS,
		    params->p, params->q,
		    params->g, pub);
}

struct lsh_string *
format_dsa_spki_key(const struct dsa_params *params,
		    const mpz_t public, const mpz_t private)
{
  struct nettle_buffer buffer;
  size_t length = dsa_keypair_to_sexp(NULL, "dsa", params, public, private);
  struct lsh_string *s = lsh_string_alloc(length);
  lsh_string_nettle_buffer(s, &buffer);
  length = dsa_keypair_to_sexp(&buffer, "dsa", params, public, private);
  assert(length == lsh_string_length(s));
  return s;
}

/* Create verifier from a key of type ssh-dss, when the atom "ssh-dss"
 * is already read from the buffer. */
struct verifier *
parse_ssh_dss_public(struct simple_buffer *buffer)
{
  NEW(dsa_verifier, res);
  dsa_params_init(&res->params);
  res->super.verify = do_dsa_verify;

  if (parse_bignum(buffer, res->params.p, DSA_SHA1_MAX_OCTETS)
      && (mpz_sgn(res->params.p) == 1)
      && parse_bignum(buffer, res->params.q, DSA_SHA1_Q_OCTETS)
      && (mpz_sgn(res->params.q) == 1)
      && mpz_sizeinbase(res->params.q, 2) == DSA_SHA1_Q_BITS
      && (mpz_cmp(res->params.q, res->params.p) < 0) /* q < p */ 
      && parse_bignum(buffer, res->params.g, DSA_SHA1_MAX_OCTETS)
      && (mpz_sgn(res->params.g) == 1)
      && (mpz_cmp(res->params.g, res->params.p) < 0) /* g < p */ 
      && parse_bignum(buffer, res->key, DSA_SHA1_MAX_OCTETS) 
      && (mpz_sgn(res->key) == 1)
      && (mpz_cmp(res->key, res->params.p) < 0) /* y < p */
      && parse_eod(buffer))
    {
      res->super.spki_key = format_dsa_spki_key(&res->params, res->key, NULL);
      return &res->super;
    }
  else
    {
      KILL(res);
      return NULL;
    }
}

  
/* Creating signatures */

static void
dsa_blob_write(struct lsh_string *buf, uint32_t pos,
	       const struct dsa_signature *signature)
{
  lsh_string_write_bignum(buf, pos, DSA_SHA1_Q_OCTETS,
			  signature->r);
  lsh_string_write_bignum(buf, pos + DSA_SHA1_Q_OCTETS, DSA_SHA1_Q_OCTETS,
			  signature->s);
}

static struct lsh_string *
do_dsa_sign(struct signer *c,
	    enum lsh_atom algorithm,
	    uint32_t msg_length,
	    const uint8_t *msg)
{
  CAST(dsa_signer, self, c);
  struct dsa_signature sv;
  struct sha1_ctx hash;
  uint8_t digest[SHA1_DIGEST_SIZE];
  struct lsh_string *signature;

  uint32_t blob_pos;
  trace("do_dsa_sign: Signing according to %a\n", algorithm);

  if (algorithm != ATOM_SSH_DSS)
    fatal("do_dsa_sign: Internal error, unexpected algorithm %a.\n",
	  algorithm);

  dsa_signature_init(&sv);
  sha1_init(&hash);
  sha1_update(&hash, msg_length, msg);
  sha1_digest(&hash, sizeof(digest), digest);

  if (dsa_sign(&self->params, self->key,
	       NULL, lsh_random, sizeof(digest), digest, &sv))
    {
      signature = ssh_format("%a%r", ATOM_SSH_DSS,
			     2 * DSA_SHA1_Q_OCTETS, &blob_pos);
      dsa_blob_write(signature, blob_pos, &sv);
    }
  else
    signature = NULL;

  dsa_signature_clear(&sv);
  return signature;
}

struct signer *
make_dsa_signer(struct sexp_iterator *i)
{
  NEW(dsa_signer, self);
  mpz_t pub;
  dsa_params_init(&self->params);
  mpz_init (pub);

  if (dsa_keypair_from_sexp_alist(&self->params, pub, self->key,
				  DSA_SHA1_MAX_BITS, DSA_SHA1_Q_BITS,
				  i))
    {
      self->super.public_key = format_dsa_public_key(&self->params, pub);
      self->super.sign = do_dsa_sign;
      
      mpz_clear(pub);
      return &self->super;
    }

  mpz_clear(pub);
  KILL(self);
  return NULL;
}

struct verifier *
make_ssh_dss_verifier(uint32_t length, const uint8_t *key)
{
  struct simple_buffer buffer;
  enum lsh_atom atom;
  
  simple_buffer_init(&buffer, length, key);

  return ( (parse_atom(&buffer, &atom)
	    && (atom == ATOM_SSH_DSS))
	   ? parse_ssh_dss_public(&buffer)
	   : NULL);
}

/* rsa.c
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 2000 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <string.h>

#include <nettle/bignum.h>
#include <nettle/rsa.h>
#include <nettle/sexp.h>
#include <nettle/sha.h>

#include "crypto.h"

#include "atoms.h"
#include "format.h"
#include "lsh_string.h"
#include "parse.h"
#include "randomness.h"
#include "sexp.h"
#include "werror.h"
#include "xalloc.h"

#include "rsa.c.x"

/* GABA:
   (class
     (name rsa_verifier)
     (super verifier)
     (vars
       (key indirect-special "struct rsa_public_key"
            #f rsa_public_key_clear)))
*/

/* GABA:
   (class
     (name rsa_signer)
     (super signer)
     (vars
       (pub indirect-special "struct rsa_public_key"
            #f rsa_public_key_clear)
       (key indirect-special "struct rsa_private_key"
            #f rsa_private_key_clear)))
*/

static int
do_rsa_verify(struct verifier *v,
	      enum lsh_atom algorithm,
	      uint32_t length,
	      const uint8_t *msg,
	      uint32_t signature_length,
	      const uint8_t *signature_data)
{
  CAST(rsa_verifier, self, v);
  struct simple_buffer buffer;
  uint32_t digits_length;
  const uint8_t *digits;
  enum lsh_atom atom;
  
  trace("do_rsa_verify: Verifying %a signature\n", algorithm);
  
  simple_buffer_init(&buffer, signature_length, signature_data);

  if ((parse_atom(&buffer, &atom)
	&& (atom == algorithm)
	&& parse_string(&buffer, &digits_length, &digits)
	/* Interop with implementations that omit leading zero bytes,
	   such as older versions of lsh. */
	&& (digits_length <= self->key.size)
	&& parse_eod(&buffer) ))
    {
      int res = 0;
      mpz_t s;
      mpz_init(s);

      nettle_mpz_set_str_256_u(s, digits_length, digits);

      switch(algorithm)
	{
	case ATOM_SSH_RSA: {
	  struct sha1_ctx hash;
	  sha1_init(&hash);
	  sha1_update(&hash, length, msg);
	  res = rsa_sha1_verify(&self->key, &hash, s);
	  debug("sha verify res: %z\n", res ? "valid": "invalid");
	  break;
	}
	case ATOM_RSA_SHA2_256: {
	  struct sha256_ctx hash;
	  sha256_init(&hash);
	  sha256_update(&hash, length, msg);
	  res = rsa_sha256_verify(&self->key, &hash, s);
	  break;
	}

	case ATOM_RSA_SHA2_512: {
	  struct sha512_ctx hash;
	  sha512_init(&hash);
	  sha512_update(&hash, length, msg);
	  res = rsa_sha512_verify(&self->key, &hash, s);
	  break;
	}

	default:
	  fatal("do_rsa_verify: Internal error!\n");
	}
      mpz_clear(s);
  
      return res;
    }
  else
    return 0;
}

struct lsh_string *
format_rsa_public_key(const struct rsa_public_key *pub)
{
  return ssh_format("%a%n%n", ATOM_SSH_RSA, pub->e, pub->n);
}

struct lsh_string *
format_rsa_spki_key(const struct rsa_public_key *public, const struct rsa_private_key *private)
{
  /* NOTE: The algorithm name "rsa-pkcs1-sha1" is the SPKI standard,
   * and what lsh-1.2 used, and is what the server-side user
   * authentication (./lsh/authorized_keys_sha1) have been using for a
   * long time. "rsa-pkcs1" makes more sense, and is what gnupg uses
   * internally (I think), and was used by some late lsh-1.3.x
   * versions.
   *
   * However, since it doesn't matter much, for now we follow the SPKI
   * standard and stay compatible with lsh-1.2. */
  struct nettle_buffer buffer;
  size_t length = rsa_keypair_to_sexp(NULL, "rsa-pkcs1-sha1", public, private);
  struct lsh_string *s = lsh_string_alloc(length);
  lsh_string_nettle_buffer(s, &buffer);
  length = rsa_keypair_to_sexp(&buffer, "rsa-pkcs1-sha1", public, private);
  assert(length == lsh_string_length(s));
  return s;
}

/* Create verifier from a key of type ssh-rsa, when the atom "ssh-rsa"
 * is already read from the buffer. */
struct verifier *
parse_ssh_rsa_public(struct simple_buffer *buffer)
{
  NEW(rsa_verifier, res);
  rsa_public_key_init(&res->key);
  res->super.verify = do_rsa_verify;

  if (parse_bignum(buffer, res->key.e, RSA_MAX_OCTETS)
      && (mpz_sgn(res->key.e) == 1)
      && parse_bignum(buffer, res->key.n, RSA_MAX_OCTETS)
      && (mpz_sgn(res->key.n) == 1)
      && (mpz_cmp(res->key.e, res->key.n) < 0)
      && parse_eod(buffer)
      && rsa_public_key_prepare(&res->key))
    {
      res->super.spki_key = format_rsa_spki_key(&res->key, NULL);
      return &res->super;
    }
  else
    {
      KILL(res);
      return NULL;
    }
}

/* Signature creation */

static struct lsh_string *
do_rsa_sign(struct signer *s,
	    enum lsh_atom algorithm,
	    uint32_t msg_length,
	    const uint8_t *msg)
{
  CAST(rsa_signer, self, s);
  struct lsh_string *res = NULL;
  mpz_t signature;
  uint32_t blob_pos;
  trace("do_rsa_sign: Signing according to %a\n", algorithm);
  
  mpz_init(signature);

  switch (algorithm)
    {
    case ATOM_SSH_RSA: {
      /* Uses sha1, and the encoding:
       *
       * string ssh-rsa
       * string signature-blob
       */
      struct sha1_ctx hash;
      sha1_init(&hash);
      sha1_update(&hash, msg_length, msg);

      if (!rsa_sha1_sign_tr(&self->pub, &self->key,
			    NULL, lsh_random,
			    &hash, signature))
	goto fail;
      break;
    }
    case ATOM_RSA_SHA2_256: {
      /* Uses sha256, and the encoding:
       *
       * string rsa-sha2-256
       * string signature-blob
       */
      struct sha256_ctx hash;
      sha256_init(&hash);
      sha256_update(&hash, msg_length, msg);

      if (!rsa_sha256_sign_tr(&self->pub, &self->key,
			      NULL, lsh_random,
			      &hash, signature))
	goto fail;
      break;
    }
    case ATOM_RSA_SHA2_512: {
      /* Uses sha512, and the encoding:
       *
       * string rsa-sha2-512
       * string signature-blob
       */
      struct sha512_ctx hash;
      sha512_init(&hash);
      sha512_update(&hash, msg_length, msg);

      if (!rsa_sha512_sign_tr(&self->pub, &self->key,
			      NULL, lsh_random,
			      &hash, signature))
	goto fail;
      break;
    }
    default:
      fatal("do_rsa_sign: Internal error, unexpected algorithm %a!\n", algorithm);
    }
  res = ssh_format("%a%r", algorithm, self->key.size, &blob_pos);
  lsh_string_write_bignum(res, blob_pos, self->key.size, signature);

 fail:
  mpz_clear(signature);
  return res;
}

struct signer *
make_rsa_signer(struct sexp_iterator *i)
{
  NEW(rsa_signer, self);

  rsa_public_key_init(&self->pub);
  rsa_private_key_init(&self->key);

  if (rsa_keypair_from_sexp_alist(&self->pub, &self->key, RSA_MAX_BITS, i)
      && rsa_public_key_prepare(&self->pub)
      && rsa_private_key_prepare(&self->key)
      && self->key.size == self->pub.size)
    {
      self->super.public_key = format_rsa_public_key(&self->pub);
      self->super.sign = do_rsa_sign;

      return &self->super;
    }
  KILL(self);
  return NULL;
}

struct verifier *
make_ssh_rsa_verifier(uint32_t length, const uint8_t *key)
{
  struct simple_buffer buffer;
  enum lsh_atom atom;
  
  simple_buffer_init(&buffer, length, key);

  return ( (parse_atom(&buffer, &atom)
	    && (atom == ATOM_SSH_RSA))
	   ? parse_ssh_rsa_public(&buffer)
	   : NULL);
}

/* curve25519_exchange.c
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 1998, 2022 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#include "crypto.h"
#include "format.h"
#include "lsh_string.h"
#include "randomness.h"
#include <nettle/curve25519.h>

void
curve25519_generate_secret(struct curve25519_dh_key *key)
{
  random_generate(sizeof(key->secret), key->secret);
  curve25519_mul_g(key->p, key->secret);
}

struct lsh_string *
curve25519_shared_secret(const uint8_t *n, const uint8_t *p)
{
  uint8_t q[CURVE25519_SIZE+1];
  unsigned i;
  q[0] = 0;
  curve25519_mul(q+1, n, p);

  /* Find first non-zero byte. */
  for (i = 1; i <= CURVE25519_SIZE; i++)
    if (q[i] > 0)
      {
	if (q[i] & 0x80)
	  /*  Keep previous zero byte. */
	  i--;
	return ssh_format("%ls", CURVE25519_SIZE + 1 - i, q + i);
      }
  return NULL;
}

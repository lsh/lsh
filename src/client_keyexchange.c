/* client_keyexchange.c
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 1998, 2005 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#include "keyexchange.h"

#include "atoms.h"
#include "crypto.h"
#include "format.h"
#include "lsh_string.h"
#include "parse.h"
#include "ssh.h"
#include "transport.h"
#include "werror.h"
#include "xalloc.h"

#include <nettle/curve25519.h>

#include "client_keyexchange.c.x"

static void
client_keyexchange_finish(struct transport_connection *connection,
			  struct lsh_string *K, struct lsh_string *exchange_hash_data,
			  struct verifier *v,
			  int hostkey_algorithm, uint32_t signature_length, const uint8_t *signature)
{
  struct lsh_string *exchange_hash;
  debug("Session key: %xS\n", K);

  exchange_hash = keyexchange_exchange_hash(&connection->kex, exchange_hash_data);

  debug("Exchange hash: %xS\n", exchange_hash);

  if (!VERIFY(v, hostkey_algorithm,
	      lsh_string_length(exchange_hash),
	      lsh_string_data(exchange_hash),
	      signature_length, signature))
	{
	  transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			       "Invalid server signature");
	  lsh_string_free(K);
	  lsh_string_free(exchange_hash);
	  return;
	}

  transport_keyexchange_finish(connection, exchange_hash, K);
}

/* GABA:
   (class
     (name client_dh_exchange)
     (super keyexchange_algorithm)
     (vars
       (params const object dh_params)
       (db object lookup_verifier)))
*/

/* Handler for the KEXDH_REPLY message */
/* GABA:
   (class
     (name client_dh_handler)
     (super transport_handler)
     (vars
       (params const object dh_params)
       (e bignum)      ; Client's public dh value
       (secret bignum) ; Client's secret exponent
       (db object lookup_verifier)
       (hostkey_algorithm . int)))
*/

static void
client_dh_handler(struct transport_handler *s,
		  struct transport_connection *connection,
		  uint32_t length, const uint8_t *packet)
{
  CAST(client_dh_handler, self, s);
  struct simple_buffer buffer;
  uint32_t key_length;
  const uint8_t *key;
  uint32_t signature_length;
  const uint8_t *signature;
  mpz_t f;
  
  trace("client_dh_handler\n");

  assert(length > 0);
  if (packet[0] != SSH_MSG_KEXDH_REPLY)
    {
      transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			   "No KEXDH_REPLY message.");
      return;
    }

  simple_buffer_init(&buffer, length-1, packet+1);
  mpz_init(f);
  if (parse_string(&buffer, &key_length, &key)
      && parse_bignum(&buffer, f, self->params->limit)
      && (mpz_cmp_ui(f, 1) > 0)
      && (mpz_cmp(f, self->params->modulo) < 0)
      && parse_string(&buffer, &signature_length, &signature)
      && parse_eod(&buffer))
    {
      struct lsh_string *K;
      mpz_t tmp;
      struct verifier *v = LOOKUP_VERIFIER(self->db, self->hostkey_algorithm,
					   key_length, key);
      if (!v)
	{
	  transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			       "Unknown server host key");
	  mpz_clear(f);
	  return;
	}

      /* Construct key */
      mpz_init(tmp);

      mpz_powm(tmp, f, self->secret, self->params->modulo);
      K = ssh_format("%ln", tmp);
      mpz_clear(tmp);

      client_keyexchange_finish(connection, K,
				ssh_format("%s%n%n%S", key_length, key,
					   self->e, f, K),
				v, self->hostkey_algorithm,
				signature_length, signature);
    }
  else
    transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			 "Invalid KEXDH_REPLY message");
  mpz_clear(f);
}

static struct transport_handler *
client_dh_init(struct keyexchange_algorithm *s,
	       struct transport_connection *connection)
{
  CAST(client_dh_exchange, self, s);
  NEW(client_dh_handler, handler);

  /* Initialize */
  handler->super.handler = client_dh_handler;
  handler->params = self->params;
  handler->db = self->db;
  handler->hostkey_algorithm = connection->kex.hostkey_algorithm;
  
  /* Generate clients 's secret exponent */
  dh_generate_secret(self->params, handler->secret, handler->e);

  /* Send client's message */
  transport_send_packet(connection, 1,
			ssh_format("%c%n", SSH_MSG_KEXDH_INIT, handler->e));

  /* Install handler */
  return &handler->super;
}


struct keyexchange_algorithm *
make_client_dh_exchange(const struct dh_params *params,
			const struct nettle_hash *H,
			struct lookup_verifier *db)
{
  NEW(client_dh_exchange, self);

  self->super.init = client_dh_init;
  self->super.H = H;
  self->params = params;
  self->db = db;
  
  return &self->super;
}

/* GABA:
   (class
     (name client_curve25519_exchange)
     (super keyexchange_algorithm)
     (vars
       (db object lookup_verifier)))
*/

/* Handler for the KEX_ECDH_REPLY message */
/* GABA:
   (class
     (name client_curve25519_handler)
     (super transport_handler)
     (vars
       (dh_key . "struct curve25519_dh_key")
       (db object lookup_verifier)
       (hostkey_algorithm . int)))
*/

static void
client_curve25519_handler(struct transport_handler *s,
			  struct transport_connection *connection,
			  uint32_t length, const uint8_t *packet)
{
  CAST(client_curve25519_handler, self, s);
  struct simple_buffer buffer;
  uint32_t key_length;
  const uint8_t *key;
  uint32_t f_length;
  const uint8_t *f;
  uint32_t signature_length;
  const uint8_t *signature;

  trace("client_curve25519_handler\n");

  assert(length > 0);
  if (packet[0] != SSH_MSG_KEX_ECDH_REPLY)
    {
      transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			   "No KEX_ECDH_REPLY message.");
      return;
    }

  simple_buffer_init(&buffer, length-1, packet+1);
  if (parse_string(&buffer, &key_length, &key)
      && parse_string(&buffer, &f_length, &f)
      && f_length == CURVE25519_SIZE
      && parse_string(&buffer, &signature_length, &signature)
      && parse_eod(&buffer))
    {
      struct lsh_string *K;

      struct verifier *v = LOOKUP_VERIFIER(self->db, self->hostkey_algorithm,
					   key_length, key);
      if (!v)
	{
	  transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			       "Unknown server host key");
	  return;
	}

      /* Derive shared secret */
      K = curve25519_shared_secret(self->dh_key.secret, f);
      if (!K)
	{
	  werror("Invalid, all-zero, shared secret!\n");
	  transport_disconnect(connection,
			       SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			       "Curve25519 failed (all-zero shared secret).");
	}
      else
	{
	  client_keyexchange_finish(connection, K,
				    ssh_format("%s%s%s%S", key_length, key,
					       CURVE25519_SIZE, self->dh_key.p,
					       CURVE25519_SIZE, f, K),
				    v, self->hostkey_algorithm,
				    signature_length, signature);
	}
    }
  else
    transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			 "Invalid KEX_ECDH_REPLY message");
}

static struct transport_handler *
client_curve25519_init(struct keyexchange_algorithm *s,
		       struct transport_connection *connection)
{
  CAST(client_curve25519_exchange, self, s);
  NEW(client_curve25519_handler, handler);

  /* Initialize */
  handler->super.handler = client_curve25519_handler;
  handler->db = self->db;
  handler->hostkey_algorithm = connection->kex.hostkey_algorithm;

  /* Generate clients 's secret exponent */
  curve25519_generate_secret(&handler->dh_key);

  /* Send client's message */
  transport_send_packet(connection, 1,
			ssh_format("%c%s", SSH_MSG_KEX_ECDH_INIT,
				   CURVE25519_SIZE, handler->dh_key.p));

  /* Install handler */
  return &handler->super;
}


struct keyexchange_algorithm *
make_client_curve25519_exchange(struct lookup_verifier *db)
{
  NEW(client_curve25519_exchange, self);

  self->super.init = client_curve25519_init;
  self->super.H = &nettle_sha256;
  self->db = db;

  return &self->super;
}

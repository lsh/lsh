/* eddsa.c */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 2024 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <string.h>

#include <nettle/eddsa.h>
#include <nettle/sexp.h>

#include "atoms.h"
#include "crypto.h"
#include "format.h"
#include "lsh_string.h"
#include "parse.h"
#include "spki.h"
#include "werror.h"
#include "xalloc.h"

#include "eddsa.c.x"

/* GABA:
   (class
     (name ed25519_verifier)
     (super verifier)
     (vars
       (key array uint8_t ED25519_KEY_SIZE)))
*/

/* GABA:
   (class
     (name ed25519_signer)
     (super signer)
     (vars
       (pub array uint8_t ED25519_KEY_SIZE)
       (key array uint8_t ED25519_KEY_SIZE)))
*/

static int
do_ed25519_verify(struct verifier *v,
		  enum lsh_atom algorithm,
		  uint32_t length,
		  const uint8_t *msg,
		  uint32_t signature_length,
		  const uint8_t *signature_data)
{
  CAST(ed25519_verifier, self, v);
  struct simple_buffer buffer;
  uint32_t ed25519_length;
  const uint8_t *ed25519;
  enum lsh_atom atom;

  trace("do_ed25519_verify: Verifying %a signature\n", algorithm);

  if (algorithm != ATOM_SSH_ED25519)
    fatal("do_ed25519_verify: Internal error!\n");

  simple_buffer_init(&buffer, signature_length, signature_data);

  return ((parse_atom(&buffer, &atom)
	  && (atom == ATOM_SSH_ED25519)
	  && parse_string(&buffer, &ed25519_length, &ed25519)
	  && ed25519_length == ED25519_SIGNATURE_SIZE
	  && parse_eod(&buffer))
	  && ed25519_sha512_verify(self->key, length, msg, ed25519));
}

struct verifier *
make_ssh_ed25519_verifier(uint32_t length, const uint8_t *key)
{
  struct simple_buffer buffer;
  enum lsh_atom atom;
  const uint8_t *ed25519;
  uint32_t ed25519_length;

  simple_buffer_init(&buffer, length, key);

  if (parse_atom(&buffer, &atom)
      && atom == ATOM_SSH_ED25519
      && parse_string(&buffer, &ed25519_length, &ed25519)
      && ed25519_length == ED25519_KEY_SIZE
      && parse_eod(&buffer))
    {
      NEW(ed25519_verifier, self);
      self->super.spki_key = format_ed25519_spki_key(ed25519, NULL);
      self->super.verify = do_ed25519_verify;
      memcpy (self->key, ed25519, sizeof (self->key));
      return &self->super;
    }
  else
    return NULL;
}

struct lsh_string *
format_ed25519_public_key(const uint8_t *pub)
{
  return ssh_format("%a%s", ATOM_SSH_ED25519, ED25519_KEY_SIZE, pub);
}

struct lsh_string *
format_ed25519_spki_key(const uint8_t *public, const uint8_t *private)
{
  struct nettle_buffer buffer;
  size_t length = ed25519_keypair_to_sexp(NULL, public, private);
  struct lsh_string *s = lsh_string_alloc(length);
  lsh_string_nettle_buffer(s, &buffer);
  length = ed25519_keypair_to_sexp(&buffer, public, private);
  assert(length == lsh_string_length(s));
  return s;
}

static struct lsh_string *
do_ed25519_sign(struct signer *c,
		enum lsh_atom algorithm,
		uint32_t msg_length,
		const uint8_t *msg)
{
  CAST(ed25519_signer, self, c);
  uint8_t ed25519[ED25519_SIGNATURE_SIZE];

  trace("do_ed25519_sign: Signing according to %a\n", algorithm);

  if (algorithm != ATOM_SSH_ED25519)
    fatal("do_ed25519_sign: Internal error, unexpected algorithm %a.\n",
	  algorithm);

  /* FIXME: Avoid copy. */
  ed25519_sha512_sign(self->pub, self->key, msg_length, msg, ed25519);
  return ssh_format("%a%s", ATOM_SSH_ED25519,
		    ED25519_SIGNATURE_SIZE, ed25519);
}

/* Accepts an alist (A |...|) (k |...|) */
struct signer *
make_ed25519_signer(struct sexp_iterator *i)
{
  const uint8_t *pub;
  const uint8_t *key;

  if (ed25519_keypair_from_sexp_alist(&pub, &key, i))
    {
      NEW(ed25519_signer, self);
      self->super.public_key = format_ed25519_public_key(pub);
      self->super.sign = do_ed25519_sign;
      memcpy (self->key, key, sizeof (self->key));
      memcpy (self->pub, pub, sizeof (self->pub));
      return &self->super;
    }
  else
    return NULL;
}

arcfour-test$(EXEEXT): arcfour-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) arcfour-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o arcfour-test$(EXEEXT)

aes-test$(EXEEXT): aes-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) aes-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o aes-test$(EXEEXT)

blowfish-test$(EXEEXT): blowfish-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) blowfish-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o blowfish-test$(EXEEXT)

cast128-test$(EXEEXT): cast128-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) cast128-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o cast128-test$(EXEEXT)

des-test$(EXEEXT): des-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) des-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o des-test$(EXEEXT)

serpent-test$(EXEEXT): serpent-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) serpent-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o serpent-test$(EXEEXT)

twofish-test$(EXEEXT): twofish-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) twofish-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o twofish-test$(EXEEXT)

md5-test$(EXEEXT): md5-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) md5-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o md5-test$(EXEEXT)

sha1-test$(EXEEXT): sha1-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) sha1-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o sha1-test$(EXEEXT)

rsa-test$(EXEEXT): rsa-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) rsa-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o rsa-test$(EXEEXT)

dsa-test$(EXEEXT): dsa-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) dsa-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o dsa-test$(EXEEXT)

ed25519-test$(EXEEXT): ed25519-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) ed25519-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o ed25519-test$(EXEEXT)

server-config-test$(EXEEXT): server-config-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) server-config-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o server-config-test$(EXEEXT)

spki-tag-test$(EXEEXT): spki-tag-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) spki-tag-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o spki-tag-test$(EXEEXT)

string-test$(EXEEXT): string-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) string-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o string-test$(EXEEXT)

sockaddr2info-test$(EXEEXT): sockaddr2info-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) sockaddr2info-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o sockaddr2info-test$(EXEEXT)

utf8-test$(EXEEXT): utf8-test.$(OBJEXT) $(TEST_OBJS)
	$(LINK) utf8-test.$(OBJEXT) $(TEST_OBJS) -lspki -lhogweed -lnettle -lutil -lcrypt -lXau -loop -lgmp  -largp -o utf8-test$(EXEEXT)


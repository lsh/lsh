#include "testutils.h"

int
test_main(void)
{
  test_sign("DSA signing",
	    S("{KDExOnByaXZhdGUta2V5KDM6ZHNhKDE6cDEyOToAg9mnws4qkXn0PNs7/+feDw7vJt1d+uR"
	      "NUxvA3kVjTSwHy5KbDb4Q2lgAcOar+7hBXES/9XC4rXed9lOq2X3HveuBXX6IED5hYG7T2K"
	      "KV+/00DS1J4iCDPrrOVRHiLE8Cl+01HplI+oSOnI+tt7R7zEfe9CVbXh1eECFbO1WguF8pK"
	      "DE6cTIxOgCCZuDer0YCC6SNQQylgPOpeGKbXSkoMTpnMTI4OjDTS7nzdr7JRxVK/kB2vH01"
	      "nJ0y9Ucd276NapQcR/qdxPMlcxUdu0qlnrmJt0rDa7YxCl6LWAUBZV2R85PaoZOuEwMEm4f"
	      "+uwk9wEBLU7TF2iRjMA+cWxVteIxKzo7Lud0AwY2ZU38lWsAl0HTYlKYHy+MCOhJ271VpFq"
	      "M/feVDKSgxOnkxMjg6ZEAgSLJ/OfQEpUaoSQnJwOni3RU6hJlGEGKJJZjTCvJ64878K3APt"
	      "tB3OQqDvcrXihKZSHyWI7tirwyFo9+e8e4sDWZljh/TKDtUB/bNMO5+YVT61Bpqiw9chsWs"
	      "zBEnv3yaXWutywEhgMtipVxeF9bTUozb4ALM7hMcG4aGf3opKDE6eDIwOlbG76+HjQbu8h3"
	      "AcPq3HabsHjCmKSkp}"),
	    S("Needs some randomness."),
	    ATOM_SSH_DSS);
  SUCCESS();
}

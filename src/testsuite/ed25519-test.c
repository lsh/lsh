#include "testutils.h"

int
test_main(void)
{
  test_sign("ED25519 signing",
	    S("{KDExOnByaXZhdGUta2V5KDc6ZWQyNTUxOSgxOkEzMjpICPuFOOdyJWE9jie3W/mmk2YFUtu"
	      "Vv4Wpk/psv1d5sCkoMTprMzI6PGTZi3j/gRBnvddHq15HMxDNbw+kFh2CNp31MIYau0IpKSk=}"),
	    S("The magic words are squeamish ossifrage"),
	    ATOM_SSH_ED25519);

  SUCCESS();
}

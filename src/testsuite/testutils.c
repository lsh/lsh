#include "testutils.h"

#include "algorithms.h"
#include "format.h"
#include "randomness.h"
#include "spki.h"
#include "werror.h"
#include "xalloc.h"

#include <nettle/knuth-lfib.h>
#include <nettle/sexp.h>

#include "spki/certificate.h"
#include "spki/parse.h"
#include "spki/tag.h"

/* -1 means invalid */
static const signed char hex_digits[0x100] =
  {
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
     0, 1, 2, 3, 4, 5, 6, 7, 8, 9,-1,-1,-1,-1,-1,-1,
    -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
  };

static unsigned
decode_hex_length(const char *h)
{
  const unsigned char *hex = (const unsigned char *) h;
  unsigned count;
  unsigned i;
  
  for (count = i = 0; hex[i]; i++)
    {
      if (isspace(hex[i]))
	continue;
      if (hex_digits[hex[i]] < 0)
	abort();
      count++;
    }

  if (count % 2)
    abort();
  return count / 2;  
}

struct lsh_string *
decode_hex(const unsigned char *hex)
{  
  uint32_t length = decode_hex_length(hex);
  
  unsigned i = 0;
  struct lsh_string *s = lsh_string_alloc(length);

  for (;;)
    {
      int high, low;
    
      while (*hex && isspace(*hex))
	hex++;

      if (!*hex)
	{
	  ASSERT(i == length);
	  return s;
	}

      high = hex_digits[*hex++];
      if (high < 0)
	return NULL;

      while (*hex && isspace(*hex))
	hex++;

      if (!*hex)
	return NULL;

      low = hex_digits[*hex++];
      if (low < 0)
	return NULL;

      lsh_string_putc(s, i++, (high << 4) | low);
    }
}

int
main(int argc, char **argv)
{
  (void) argc; (void) argv;
  return test_main();
}

void
test_cipher(const char *name, struct crypto_algorithm *algorithm,
	    const struct lsh_string *key,
	    const struct lsh_string *plain,
	    const struct lsh_string *cipher,
	    const struct lsh_string *iv)
{
  struct crypto_instance *c;
  struct lsh_string *x;

  (void) name;
  
  if (iv)
    {
      if (algorithm->iv_size != lsh_string_length(iv))
	FAIL();
    }
  else if (algorithm->iv_size)
    FAIL();

  c = MAKE_ENCRYPT(algorithm, lsh_string_data(key),
		   iv ? lsh_string_data(iv) : NULL);

  x = crypt_string(c, lsh_string_dup(plain));
  if (!lsh_string_eq(x, cipher))
    FAIL();
  
  KILL(c);
  
  c = MAKE_DECRYPT(algorithm, lsh_string_data(key),
		   iv ? lsh_string_data(iv) : NULL);

  x = crypt_string(c, x);
  if (!lsh_string_eq(x, plain))
    FAIL();

  KILL(c);
  lsh_string_free(x);
}

void
test_hash(const char *name,
	  const struct nettle_hash *algorithm,
	  const struct lsh_string *data,
	  const struct lsh_string *digest)
{
  (void) name;
  if (!lsh_string_eq(hash_string(algorithm, data, 0), digest))
    FAIL();
}

void
test_mac(const char *name,
	  struct mac_algorithm *algorithm,
	  const struct lsh_string *key,
	  const struct lsh_string *data,
	  const struct lsh_string *digest)
{
  (void) name;
  if (!lsh_string_eq(mac_string(algorithm, key, 0, data, 0),
		     digest))
    FAIL();
}

/* Bous randomness generator, for testing */ 
struct knuth_lfib_ctx bad_random;

void
random_generate(uint32_t length, uint8_t *dst)
{
  knuth_lfib_random(&bad_random, length, dst);
}

void
lsh_random(void *ctx UNUSED, size_t length, uint8_t *dst)
{
  knuth_lfib_random(&bad_random, length, dst);
}

void
test_sign(const char *name,
	  const struct lsh_string *key,
	  struct lsh_string *msg,
	  int algorithm)
{
  struct lsh_string *sign;
  struct signer *s;
  struct verifier *v;
  
  knuth_lfib_init(&bad_random, time(NULL));

  (void) name;

  s = spki_make_signer(key, NULL);
  if (!s)
    FAIL();

  sign = SIGN(s, algorithm, lsh_string_length(msg), lsh_string_data(msg));
  switch (algorithm)
    {
    default:
      FAIL();
    case ATOM_SSH_DSS:
      v = make_ssh_dss_verifier(STRING_LD(s->public_key));
      break;
    case ATOM_SSH_RSA:
    case ATOM_RSA_SHA2_256:
    case ATOM_RSA_SHA2_512:
      v = make_ssh_rsa_verifier(STRING_LD(s->public_key));
      break;
    case ATOM_SSH_ED25519:
      v = make_ssh_ed25519_verifier(STRING_LD(s->public_key));
      break;
    }
  if (!v)
    /* Can't create verifier */
    FAIL();

  if (!VERIFY(v, algorithm,
	      lsh_string_length(msg), lsh_string_data(msg),
	      lsh_string_length(sign), lsh_string_data(sign)))
    /* Unexpected verification failure. */
    FAIL();
  
  /* Modify message slightly. */
  if (lsh_string_length(msg) < 10)
    FAIL();

  lsh_string_putc(msg, 5, lsh_string_data(msg)[5] ^ 0x40);

  if (VERIFY(v, algorithm,
	     lsh_string_length(msg), lsh_string_data(msg),
	     lsh_string_length(sign), lsh_string_data(sign)))
    /* Unexpected verification success. */
    FAIL();

  lsh_string_free(sign);
  
  KILL(v);
  KILL(s);
}

static int
test_spki_match(const char *name,
		const struct lsh_string *resource,
		const struct lsh_string *access)
{
  struct spki_acl_db db;
  struct spki_iterator i;
  struct spki_tag *resource_tag;
  struct spki_tag *access_tag;
  int result;
  
  spki_acl_init(&db);
  
  if (!spki_iterator_first(&i, STRING_LD(resource)))
    FAIL();
  
  if (!spki_parse_tag(&db, &i, &resource_tag))
    FAIL();

  if (!spki_iterator_first(&i, STRING_LD(access)))
    FAIL();

  if (!spki_parse_tag(&db, &i, &access_tag))
    FAIL();
  
  (void) name;

  result = spki_tag_includes(resource_tag, access_tag);

  spki_tag_free(&db, resource_tag);
  spki_tag_free(&db, access_tag);
  spki_acl_clear(&db);
  
  return result;
}

void
test_spki_grant(const char *name,
		const struct lsh_string *resource,
		const struct lsh_string *access)
{
  if (!test_spki_match(name, resource, access))
    FAIL();
}

void
test_spki_deny(const char *name,
	       const struct lsh_string *resource,
	       const struct lsh_string *access)
{
  if (test_spki_match(name, resource, access))
    FAIL();
}

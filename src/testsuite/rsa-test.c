#include "testutils.h"

int
test_main(void)
{
  test_sign("RSA sha1 signing",
	    S("{KDExOnByaXZhdGUta2V5KDE0OnJzYS1wa2NzMS1zaGExKDE6bjI1MzoBFRr6hWOxSyj4UOu"
	      "RSv2TgnQTRIbT+XhBCCuC0delL8hCt9SKN8QYGn9eSfJaMknWXZdbiATciNWXtC6BInC5Pi"
	      "IyCwZP21u4x/UYNq7EpYRWfOr2GeFtgqAgjLCPiNcDmaje5RI6HltB1t69gyRlgmTEGedjf"
	      "CVUksR9HCa/YHO4Qj6nfGL43suz0qzbw90fo52pJQAFVsob8uNXcIkKEIUF6iaCcFxz5chl"
	      "mUDDuWZsu0CN2hwTVcNEODo2h4K9UZSJ1q5rflh3Yd2hKeQDY4g1em/4faS43lvhkV+PqXN"
	      "yaV5/9Oo5PfPhuVbppOvhV3fU6GsPNH2rmaSnKSgxOmU0Og8eiqcpKDE6ZDI1MzoBALlbGg"
	      "NWW/Iobv5Zm17Ilj8Vwg3DgYucIlNkKs4z+G5I1J5wSLlsSJp5mDixgy2meph8Zo7kRM8DX"
	      "NbQFxPrx184sRHI+zQlcd61ldDSiy+U55cw99k+AW9xPGiSlIwfGFJb8+ihimFrLD4bpJTj"
	      "ZmWFMza4W4EqovJZar2Lk8j1ahEClrVZxmClMwGViQf6sezat9eDccA3D1J7lbCqhhZ4/zO"
	      "7M97VifRfcvzJCRsQUgf3fdsY6BFp0/jkx+7y0ZgCIfBx7JJPPAe90wNIG7s+XXE4A0M3tx"
	      "usqFE3gpIZ+H84xD/as/h/CAkDnZDKxTjaeL9KEMXaJDlXKSgxOnAxMjc6AMDrJe3pnACcC"
	      "kZDTxmNifj/107wgUsyqCX097sHwkRKRn4OGJYg1V8odSIBXiGVO9HvDqEZKwZQ7ck83IHa"
	      "vaJIEnTy7vrSH3wS3x60PmQHINlqd3CnJISIKsvBnULrU8P892gxqTpqCEvpoo1ZnQzZqYN"
	      "iAf5kj47TqJN2lykoMTpxMTI3OgFvtvQgJp9z2OJ+jJRPY3xPxK1wkymRYSBAEjSTxfs7Ci"
	      "UjQ7cdWXlZoF/P9NGRVQErNkknFpVEC2Mh1qpIw4wN1ES/Z54XTgshkibn2+wtDpD0Vj5r5"
	      "S3UIta6iCn39SJbAUQSuMuuLLj5jwCfq8d/V7BaU7aO58244vcFlHEpKDE6YTEyNjoiG57+"
	      "FA/nD4jGoPHo5LPNSxecFmC8YlC9r0ATzEB3xFW6g7qY8QYw2h/zmarUqO5KQVDxHwk+kJ2"
	      "4BIqMxDyqFdhjr7c3bg7jSBv3MqSGfNWu/awoQ5rffKPDKN8TzWjrUJHYKQBjODfzmZuuWA"
	      "sHeEb4u7E4K8f3NLs14nMpKDE6YjEyNjouubKPLYPvxC3dj4WlXwRhfRgD2F/Dk2T00eEyJ"
	      "st/LLHFvNQ8H5/iB1FmUGWVcZAheYNFp2RzNU2+MoOgskKFQJ4qVqX/A9mH9BULiEXMG1MC"
	      "7hFT0O/w2bFBz6dFtpb50nPfO09J/G+DON1TnoOjIxJ0RLy5Uq45jx9vPGcpKDE6YzEyNzo"
	      "As20M6PMchs+bdN/hiVdzD0PG5eOFBCjR1tUbzRvhcA9zRW1ze0ozDyF1ZuqIqnoniHMz5T"
	      "m7P9swIioNNgrWys/Sk1nyPdDw6zkEj8mtW2RUyXb8FK+rgtE9UjTwTj2gZEsj8TjPE5Wua"
	      "agBKe2KtQ/OuMsYG7bMADddMRv4KSkp}"),
	    S("The magic words are squeamish ossifrage"),
	    ATOM_SSH_RSA);

  test_sign("RSA sha256 signing",
	    S("{KDExOnByaXZhdGUta2V5KDE0OnJzYS1wa2NzMS1zaGExKDE6bjI1MzoBFRr6hWOxSyj4UOu"
	      "RSv2TgnQTRIbT+XhBCCuC0delL8hCt9SKN8QYGn9eSfJaMknWXZdbiATciNWXtC6BInC5Pi"
	      "IyCwZP21u4x/UYNq7EpYRWfOr2GeFtgqAgjLCPiNcDmaje5RI6HltB1t69gyRlgmTEGedjf"
	      "CVUksR9HCa/YHO4Qj6nfGL43suz0qzbw90fo52pJQAFVsob8uNXcIkKEIUF6iaCcFxz5chl"
	      "mUDDuWZsu0CN2hwTVcNEODo2h4K9UZSJ1q5rflh3Yd2hKeQDY4g1em/4faS43lvhkV+PqXN"
	      "yaV5/9Oo5PfPhuVbppOvhV3fU6GsPNH2rmaSnKSgxOmU0Og8eiqcpKDE6ZDI1MzoBALlbGg"
	      "NWW/Iobv5Zm17Ilj8Vwg3DgYucIlNkKs4z+G5I1J5wSLlsSJp5mDixgy2meph8Zo7kRM8DX"
	      "NbQFxPrx184sRHI+zQlcd61ldDSiy+U55cw99k+AW9xPGiSlIwfGFJb8+ihimFrLD4bpJTj"
	      "ZmWFMza4W4EqovJZar2Lk8j1ahEClrVZxmClMwGViQf6sezat9eDccA3D1J7lbCqhhZ4/zO"
	      "7M97VifRfcvzJCRsQUgf3fdsY6BFp0/jkx+7y0ZgCIfBx7JJPPAe90wNIG7s+XXE4A0M3tx"
	      "usqFE3gpIZ+H84xD/as/h/CAkDnZDKxTjaeL9KEMXaJDlXKSgxOnAxMjc6AMDrJe3pnACcC"
	      "kZDTxmNifj/107wgUsyqCX097sHwkRKRn4OGJYg1V8odSIBXiGVO9HvDqEZKwZQ7ck83IHa"
	      "vaJIEnTy7vrSH3wS3x60PmQHINlqd3CnJISIKsvBnULrU8P892gxqTpqCEvpoo1ZnQzZqYN"
	      "iAf5kj47TqJN2lykoMTpxMTI3OgFvtvQgJp9z2OJ+jJRPY3xPxK1wkymRYSBAEjSTxfs7Ci"
	      "UjQ7cdWXlZoF/P9NGRVQErNkknFpVEC2Mh1qpIw4wN1ES/Z54XTgshkibn2+wtDpD0Vj5r5"
	      "S3UIta6iCn39SJbAUQSuMuuLLj5jwCfq8d/V7BaU7aO58244vcFlHEpKDE6YTEyNjoiG57+"
	      "FA/nD4jGoPHo5LPNSxecFmC8YlC9r0ATzEB3xFW6g7qY8QYw2h/zmarUqO5KQVDxHwk+kJ2"
	      "4BIqMxDyqFdhjr7c3bg7jSBv3MqSGfNWu/awoQ5rffKPDKN8TzWjrUJHYKQBjODfzmZuuWA"
	      "sHeEb4u7E4K8f3NLs14nMpKDE6YjEyNjouubKPLYPvxC3dj4WlXwRhfRgD2F/Dk2T00eEyJ"
	      "st/LLHFvNQ8H5/iB1FmUGWVcZAheYNFp2RzNU2+MoOgskKFQJ4qVqX/A9mH9BULiEXMG1MC"
	      "7hFT0O/w2bFBz6dFtpb50nPfO09J/G+DON1TnoOjIxJ0RLy5Uq45jx9vPGcpKDE6YzEyNzo"
	      "As20M6PMchs+bdN/hiVdzD0PG5eOFBCjR1tUbzRvhcA9zRW1ze0ozDyF1ZuqIqnoniHMz5T"
	      "m7P9swIioNNgrWys/Sk1nyPdDw6zkEj8mtW2RUyXb8FK+rgtE9UjTwTj2gZEsj8TjPE5Wua"
	      "agBKe2KtQ/OuMsYG7bMADddMRv4KSkp}"),
	    S("The magic words are squeamish ossifrage"),
	    ATOM_RSA_SHA2_256);
  test_sign("RSA sha512 signing",
	    S("{KDExOnByaXZhdGUta2V5KDE0OnJzYS1wa2NzMS1zaGExKDE6bjI1MzoBFRr6hWOxSyj4UOu"
	      "RSv2TgnQTRIbT+XhBCCuC0delL8hCt9SKN8QYGn9eSfJaMknWXZdbiATciNWXtC6BInC5Pi"
	      "IyCwZP21u4x/UYNq7EpYRWfOr2GeFtgqAgjLCPiNcDmaje5RI6HltB1t69gyRlgmTEGedjf"
	      "CVUksR9HCa/YHO4Qj6nfGL43suz0qzbw90fo52pJQAFVsob8uNXcIkKEIUF6iaCcFxz5chl"
	      "mUDDuWZsu0CN2hwTVcNEODo2h4K9UZSJ1q5rflh3Yd2hKeQDY4g1em/4faS43lvhkV+PqXN"
	      "yaV5/9Oo5PfPhuVbppOvhV3fU6GsPNH2rmaSnKSgxOmU0Og8eiqcpKDE6ZDI1MzoBALlbGg"
	      "NWW/Iobv5Zm17Ilj8Vwg3DgYucIlNkKs4z+G5I1J5wSLlsSJp5mDixgy2meph8Zo7kRM8DX"
	      "NbQFxPrx184sRHI+zQlcd61ldDSiy+U55cw99k+AW9xPGiSlIwfGFJb8+ihimFrLD4bpJTj"
	      "ZmWFMza4W4EqovJZar2Lk8j1ahEClrVZxmClMwGViQf6sezat9eDccA3D1J7lbCqhhZ4/zO"
	      "7M97VifRfcvzJCRsQUgf3fdsY6BFp0/jkx+7y0ZgCIfBx7JJPPAe90wNIG7s+XXE4A0M3tx"
	      "usqFE3gpIZ+H84xD/as/h/CAkDnZDKxTjaeL9KEMXaJDlXKSgxOnAxMjc6AMDrJe3pnACcC"
	      "kZDTxmNifj/107wgUsyqCX097sHwkRKRn4OGJYg1V8odSIBXiGVO9HvDqEZKwZQ7ck83IHa"
	      "vaJIEnTy7vrSH3wS3x60PmQHINlqd3CnJISIKsvBnULrU8P892gxqTpqCEvpoo1ZnQzZqYN"
	      "iAf5kj47TqJN2lykoMTpxMTI3OgFvtvQgJp9z2OJ+jJRPY3xPxK1wkymRYSBAEjSTxfs7Ci"
	      "UjQ7cdWXlZoF/P9NGRVQErNkknFpVEC2Mh1qpIw4wN1ES/Z54XTgshkibn2+wtDpD0Vj5r5"
	      "S3UIta6iCn39SJbAUQSuMuuLLj5jwCfq8d/V7BaU7aO58244vcFlHEpKDE6YTEyNjoiG57+"
	      "FA/nD4jGoPHo5LPNSxecFmC8YlC9r0ATzEB3xFW6g7qY8QYw2h/zmarUqO5KQVDxHwk+kJ2"
	      "4BIqMxDyqFdhjr7c3bg7jSBv3MqSGfNWu/awoQ5rffKPDKN8TzWjrUJHYKQBjODfzmZuuWA"
	      "sHeEb4u7E4K8f3NLs14nMpKDE6YjEyNjouubKPLYPvxC3dj4WlXwRhfRgD2F/Dk2T00eEyJ"
	      "st/LLHFvNQ8H5/iB1FmUGWVcZAheYNFp2RzNU2+MoOgskKFQJ4qVqX/A9mH9BULiEXMG1MC"
	      "7hFT0O/w2bFBz6dFtpb50nPfO09J/G+DON1TnoOjIxJ0RLy5Uq45jx9vPGcpKDE6YzEyNzo"
	      "As20M6PMchs+bdN/hiVdzD0PG5eOFBCjR1tUbzRvhcA9zRW1ze0ozDyF1ZuqIqnoniHMz5T"
	      "m7P9swIioNNgrWys/Sk1nyPdDw6zkEj8mtW2RUyXb8FK+rgtE9UjTwTj2gZEsj8TjPE5Wua"
	      "agBKe2KtQ/OuMsYG7bMADddMRv4KSkp}"),
	    S("The magic words are squeamish ossifrage"),
	    ATOM_RSA_SHA2_512);
  SUCCESS();
}

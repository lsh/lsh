/* server_keyexchange.c
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 1998, 2005 Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>

#include "atoms.h"
#include "crypto.h"
#include "format.h"
#include "lsh_string.h"
#include "parse.h"
#include "sexp.h"
#include "ssh.h"
#include "transport.h"
#include "werror.h"
#include "xalloc.h"

#include <nettle/curve25519.h>

#include "server_keyexchange.c.x"

static void
server_keyexchange_finish(struct transport_connection *connection,
			  struct signer *server_key,
			  struct lsh_string *K,
			  struct lsh_string *exchange_hash_data,
			  struct lsh_string *packet_sans_signature)
{
  struct lsh_string *exchange_hash;
  struct lsh_string *signature;

  debug("Session key: %xS\n", K);

  exchange_hash = keyexchange_exchange_hash(&connection->kex, exchange_hash_data);
  debug("Exchange hash: %xS\n", exchange_hash);

  signature = SIGN(server_key,
		   connection->kex.hostkey_algorithm,
		   lsh_string_length(exchange_hash),
		   lsh_string_data(exchange_hash));

  if (signature)
    {
      /* Send server's message, to complete key exchange */
      transport_send_packet(connection, 0,
			    ssh_format("%lfS%fS",
				       packet_sans_signature, signature));
      transport_keyexchange_finish(connection,
				   exchange_hash,
				   K);
    }
  else
    {
      werror("Signing using the private key failed.\n");
      transport_disconnect(connection,
			   SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			   "Configuration error.");
      lsh_string_free(K);
      lsh_string_free(exchange_hash);
    }
}

/* GABA:
   (class
     (name server_dh_exchange)
     (super keyexchange_algorithm)
     (vars
       (params const object dh_params)
       (keys object alist)))
*/

/* Handler for the KEXDH_INIT message */
/* GABA:
   (class
     (name server_dh_handler)
     (super transport_handler)
     (vars
       (params const object dh_params)
       (key object signer)))
*/

static void
server_dh_handler(struct transport_handler *s,
		  struct transport_connection *connection,
		  uint32_t length, const uint8_t *packet)
{
  CAST(server_dh_handler, self, s);
  struct simple_buffer buffer;
  mpz_t e;

  trace("server_dh_handler\n");

  assert(length > 0);
  if (packet[0] != SSH_MSG_KEXDH_INIT)
    {
      transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			   "No KEXDH_INIT message.");
      return;
    }

  simple_buffer_init(&buffer, length-1, packet+1);

  mpz_init (e);
  if (parse_bignum(&buffer, e, self->params->limit)
      && (mpz_cmp_ui(e, 1) > 0)
      && (mpz_cmp(e, self->params->modulo) < 0)
      && parse_eod(&buffer) )
    {
      struct lsh_string *K;
      struct lsh_string *exchange_hash_data;
      struct lsh_string *reply;
      mpz_t secret, f;

      mpz_init(secret);
      mpz_init(f);

      /* Generate server's secret exponent */
      dh_generate_secret (self->params, secret, f);

      mpz_powm(secret, e, secret, self->params->modulo);
      K = ssh_format("%ln", secret);

      exchange_hash_data = ssh_format("%S%n%n%S", self->key->public_key,
				      e, f, K);

      reply = ssh_format("%c%S%n",
			 SSH_MSG_KEXDH_REPLY,
			 self->key->public_key,
			 f);
      mpz_clear(secret);
      mpz_clear(f);

      server_keyexchange_finish(connection, self->key, K, exchange_hash_data, reply);
    }
  else
    transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			 "Invalid KEXDH_INIT message.");
  mpz_clear(e);
}  

static struct transport_handler *
server_dh_init(struct keyexchange_algorithm *s,
	       struct transport_connection *connection)
{
  CAST(server_dh_exchange, self, s);
  CAST_SUBTYPE(signer, key, ALIST_GET(self->keys, connection->kex.hostkey_algorithm));

  if (!key)
    {
      werror("Keypair for for selected signature-algorithm not found!\n");
      return NULL;
    }
  else
    {
      NEW(server_dh_handler, handler);
      
      /* Initialize */
      handler->super.handler = server_dh_handler;
      handler->params = self->params;
      handler->key = key;

      /* Return handler */
      return &handler->super;
    }
}

struct keyexchange_algorithm *
make_server_dh_exchange(const struct dh_params *params,
			const struct nettle_hash *H,
			struct alist *keys)
{
  NEW(server_dh_exchange, self);
  self->super.init = server_dh_init;
  self->super.H = H;
  self->params = params;
  self->keys = keys;

  return &self->super;
}

/* GABA:
   (class
     (name server_curve25519_exchange)
     (super keyexchange_algorithm)
     (vars
       (keys object alist)))
*/

/* Handler for the KEXDH_INIT message */
/* GABA:
   (class
     (name server_curve25519_handler)
     (super transport_handler)
     (vars
       (key object signer)))
*/

static void
server_curve25519_handler(struct transport_handler *s,
			  struct transport_connection *connection,
			  uint32_t length, const uint8_t *packet)
{
  CAST(server_curve25519_handler, self, s);
  struct simple_buffer buffer;
  uint32_t e_length;
  const uint8_t *e;
  trace("server_curve25519_handler\n");

  assert(length > 0);
  if (packet[0] != SSH_MSG_KEX_ECDH_INIT)
    {
      transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			   "No KEX_ECDH_INIT message.");
      return;
    }

  simple_buffer_init(&buffer, length-1, packet+1);

  if (parse_string(&buffer, &e_length, &e)
      && e_length == CURVE25519_SIZE
      && parse_eod(&buffer))
    {
      struct curve25519_dh_key dh_key;
      struct lsh_string *K;

      curve25519_generate_secret(&dh_key);
      K = curve25519_shared_secret(dh_key.secret, e);

      if (!K)
	{
	  werror("Invalid, all-zero, shared secret!\n");
	  transport_disconnect(connection,
			       SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			       "Curve25519 failed (all-zero shared secret).");
	}
      else
	{
	  server_keyexchange_finish(connection, self->key, K,
				    ssh_format("%S%s%s%S", self->key->public_key,
					       CURVE25519_SIZE, e,
					       CURVE25519_SIZE, dh_key.p, K),
				    ssh_format("%c%S%s",
					       SSH_MSG_KEX_ECDH_REPLY,
					       self->key->public_key,
					       CURVE25519_SIZE, dh_key.p));
	}
    }
  else
    transport_disconnect(connection, SSH_DISCONNECT_KEY_EXCHANGE_FAILED,
			 "Invalid KEXDH_INIT message.");
}

static struct transport_handler *
server_curve25519_init(struct keyexchange_algorithm *s,
		       struct transport_connection *connection)
{
  CAST(server_curve25519_exchange, self, s);
  CAST_SUBTYPE(signer, key, ALIST_GET(self->keys, connection->kex.hostkey_algorithm));

  if (!key)
    {
      werror("Keypair for for selected signature-algorithm not found!\n");
      return NULL;
    }
  else
    {
      NEW(server_curve25519_handler, handler);

      /* Initialize */
      handler->super.handler = server_curve25519_handler;
      handler->key = key;

      /* Return handler */
      return &handler->super;
    }
}

struct keyexchange_algorithm *
make_server_curve25519_exchange(struct alist *keys)
{
  NEW(server_curve25519_exchange, self);
  self->super.init = server_curve25519_init;
  self->super.H = &nettle_sha256;
  self->keys = keys;

  return &self->super;
}

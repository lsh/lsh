/* spki.c
 *
 * An implementation of SPKI certificate checking
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 1999, 2003 Balázs Scheidler, Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <string.h>

#include <nettle/dsa.h>
#include <nettle/eddsa.h>
#include <nettle/rsa.h>
#include <nettle/sexp.h>

#include "spki/parse.h"
#include "spki/tag.h"

#include "spki.h"

#include "atoms.h"
#include "crypto.h"
#include "format.h"
#include "io.h"
#include "list.h"
#include "lsh_string.h"
#include "parse.h"
#include "sexp.h"
#include "werror.h"
#include "xalloc.h"
#include "alist.h"

/* FIXME: This should create different tags for hostnames that are not
 * dns fqdn:s. */

struct lsh_string *
make_ssh_hostkey_tag(const char *host)
{
  uint32_t left = strlen(host);
  const uint8_t *s = host;
  struct lsh_string *tag;
  struct lsh_string *reversed = lsh_string_alloc(left);

  /* First, transform "foo.lysator.liu.se" into "se.liu.lysator.foo" */
  while (left)
    {
      uint8_t *p = memchr(s, '.', left);
      if (!p)
	{
	  lsh_string_write(reversed, 0, left, s);
	  break;
	}
      else
	{
	  uint32_t segment = p - s;
	  left -= segment;

	  lsh_string_write(reversed, left, segment, s);
	  lsh_string_putc(reversed, --left, '.');
	  s = p+1;
	}
    }

  tag = lsh_string_format_sexp("(tag(ssh-hostkey%s))",
			       STRING_LD(reversed));
  lsh_string_free(reversed);

  return tag;
}      

/* Syntax: (<algorithm> ...). Advances the iterator past the algorithm
 * identifier, and returns the corresponding algorithm. */
const struct lsh_object *
spki_algorithm_lookup(struct alist *algorithms,
		      struct sexp_iterator *i,
		      int *type)
{
  struct lsh_object *res;
  enum lsh_atom algorithm_name = lsh_sexp_get_type(i);

  /* FIXME: Display a pretty message if lookup fails. */
  res = ALIST_GET(algorithms, algorithm_name);

  if (res && type)
    *type = algorithm_name;

  return res;
}

/* Syntax: (<algorithm> ...). Advances the iterator past the algorithm
 * identifier, and returns the corresponding key type, ssh-rsa or ssh-dss. */
/* FIXME: Should really be replaced with spki_parse_type. */
enum lsh_atom
spki_key_type_lookup(struct sexp_iterator *i)
{
  static const char * const types[4] = { "dsa", "ed25519", "rsa-pkcs1", "rsa-pkcs1-sha1" };
  const char *type;

  type = sexp_iterator_check_types(i, 4, types);
  if (type == types[0])
    return ATOM_SSH_DSS;
  if (type == types[1])
    return ATOM_SSH_ED25519;
  if (type != NULL)
    return ATOM_SSH_RSA;
  else
    return 0;
}

#define SEXP_IS_STRING_LEN(i, length) \
  ((i)->type == SEXP_ATOM			\
   && (i)->atom_length == (length)		\
   && !(i)->display)

/* Accepts an alist ((A |...|) (k |...|)) */
int
ed25519_keypair_from_sexp_alist(const uint8_t **pub, const uint8_t **priv,
				struct sexp_iterator *i)
{
  static const char * const names[2] = { "A", "k" };
  struct sexp_iterator values[2];

  if (! (sexp_iterator_assoc(i, 1 + (priv != NULL), names, values)
	 && SEXP_IS_STRING_LEN(&values[0], ED25519_KEY_SIZE)))
    return 0;

  if (priv)
    {
      if (!SEXP_IS_STRING_LEN(&values[1], ED25519_KEY_SIZE))
	return 0;
      *priv = values[1].atom;
    }
  *pub = values[0].atom;

  return 1;
}

/* Uses syntax

     (public-key (ed25519 (A |...|)))
     (private-key (ed25519 (A |...|) (k |...|)))

   with names "A" for public point and "k" for the private key,
   following RFC 8032.
*/
int
ed25519_keypair_to_sexp(struct nettle_buffer *buffer,
			const uint8_t *public, const uint8_t *private)
{
  if (private)
    return sexp_format(buffer,
		       "(private-key(ed25519(A%s)(k%s)))",
		       ED25519_KEY_SIZE, public,
		       ED25519_KEY_SIZE, private);
  else
    return sexp_format(buffer,
		       "(public-key(ed25519(A%s)))",
		       ED25519_KEY_SIZE, public);
}


struct lsh_string *
spki_to_ssh_pubkey(struct sexp_iterator *i,
		   enum lsh_atom *type)
{
  /* Syntax: (<algorithm> <s-expr>*) */
  struct lsh_string *key = NULL;

  switch ((*type = spki_key_type_lookup(i)))
    {
    case ATOM_SSH_RSA:
      {
	struct rsa_public_key pub;
	rsa_public_key_init(&pub);
	if (rsa_keypair_from_sexp_alist(&pub, NULL, RSA_MAX_BITS, i)
	    && rsa_public_key_prepare(&pub))
	  key = format_rsa_public_key(&pub);
	rsa_public_key_clear(&pub);
	break;
      }
    case ATOM_SSH_DSS:
      {
	struct dsa_params params;
	mpz_t pub;
	dsa_params_init(&params);
	mpz_init(pub);
	if (dsa_keypair_from_sexp_alist(&params, pub, NULL,
					DSA_SHA1_MAX_BITS, DSA_SHA1_Q_BITS,
					i))
	  key = format_dsa_public_key(&params, pub);
	dsa_params_clear(&params);
	mpz_clear(pub);
	break;
      }
    case ATOM_SSH_ED25519:
      {
	const uint8_t *pub;
	if (ed25519_keypair_from_sexp_alist(&pub, NULL, i))
	  key = format_ed25519_public_key(pub);
	break;
      }
    default:
      ;
    }
  return key;
}

/* Reading keys */
/* NOTE: With transport syntax */

struct signer *
spki_make_signer(const struct lsh_string *key,
		 enum lsh_atom *type)
{
  struct sexp_iterator i;
  struct lsh_string *decoded = lsh_string_dup(key);
  struct signer *res = NULL;
  
  if (lsh_string_transport_iterator_first(decoded, &i)
      && sexp_iterator_check_type(&i, "private-key"))
    {
      enum lsh_atom t = spki_key_type_lookup(&i);
      if (type)
	*type = t;

      switch (t) 
	{
	case ATOM_SSH_RSA:
	  res = make_rsa_signer(&i);
	  break;
	case ATOM_SSH_DSS:
	  res = make_dsa_signer(&i);
	  break;
	case ATOM_SSH_ED25519:
	  res = make_ed25519_signer(&i);
	  break;
	default:
	  werror("spki_make_signer: Unknown key type.\n");
	  break;
	}
    }
  else
    werror("spki_make_signer: Expected private-key expression.\n");

  lsh_string_free(decoded);
  if (!res)
    werror("spki_make_signer: Failed to decode private key.\n");
  return res;
}

/* The input is a string containing zero or more acls to be processed. */
int
spki_add_acls(struct spki_acl_db *db,
	      uint32_t length, const uint8_t *data)
{
  struct spki_iterator i;

  if (!spki_iterator_first(&i, length, data))
    {
      werror("spki_add_acls: S-expression syntax error.\n");
      return 0;
    }
  while (i.type != SPKI_TYPE_END_OF_EXPR)
    {
      if (!spki_acl_process(db, &i))
	{
	  werror("spki_add_acls: Invalid ACL.\n");
	  return 0;
	}
    }
  return 1;
}

int
spki_authorize(const struct spki_acl_db *db,
	       const struct spki_principal *user,
	       time_t t,
	       const struct lsh_string *access)
{
  const struct spki_5_tuple *acl;
  struct spki_date date;
  struct spki_tag *tag;
  struct spki_iterator i;
  const struct spki_5_tuple_list *p;
  
  if (!spki_iterator_first(&i, STRING_LD(access))
      || !spki_parse_tag(db, &i, &tag))
    {
      werror("spki_authorize: Invalid tag.\n");
      return 0;
    }

  spki_date_from_time_t(&date, t);
  
  for (acl = spki_acl_by_subject_first(db, &p, user);
       acl;
       acl = spki_acl_by_subject_next(&p, user))
    {
      if (spki_tag_includes(acl->tag, tag)
	  && SPKI_DATE_CMP(acl->not_before, date) <= 0
	  && SPKI_DATE_CMP(acl->not_after, date) >= 0)
	{
	  spki_tag_free(db, tag);
	  return 1;
	}
    }
  spki_tag_free(db, tag);

  return 0;
}

/* PKCS-5 handling */

/* Encryption of private data.
 * For PKCS#5 (version 2) key derivation, we use
 *
 * (password-encrypted LABEL
 *   (Xpkcs5v2 hmac-sha1 (salt #...#)
 *                       (iterations #...#))
 *   ("3des-cbc" (iv #...#) (data #...#)))
 *
 * where the X:s will be removed when the format is more stable.
 *
 */

/* Frees input string. */
struct lsh_string *
spki_pkcs5_encrypt(struct lsh_string *label,
		   uint32_t prf_name,
		   struct mac_algorithm *prf,
		   int crypto_name,
		   struct crypto_algorithm *crypto,
		   uint32_t salt_length,
		   struct lsh_string *password,
		   uint32_t iterations,
                   const struct lsh_string *data)
{
  struct lsh_string *key;
  struct lsh_string *salt;
  struct lsh_string *iv;
  const uint8_t *iv_data;
  struct lsh_string *encrypted;
  struct lsh_string *value;

  assert(crypto);
  assert(prf);

  salt = lsh_string_random(salt_length);

  key = pkcs5_derive_key(prf,
			 password, salt, iterations,
			 crypto->key_size);

  if (crypto->iv_size)
    {
      iv = lsh_string_random(crypto->iv_size);
      iv_data = lsh_string_data(iv);
    }
  else
    {
      iv = NULL;
      iv_data = NULL;
    }
  encrypted = crypt_string_pad(MAKE_ENCRYPT(crypto,
					    lsh_string_data(key),
					    iv_data),
			       data);
  
  value = lsh_string_format_sexp("(password-encrypted%s(Xpkcs5v2%0s"
				 "(iterations%i)(salt%s))"
				 "(%0s(iv%s)(data%s)))",
				 STRING_LD(label),
				 get_atom_name(prf_name),
				 iterations,
				 STRING_LD(salt),
				 get_atom_name(crypto_name),
				 crypto->iv_size, iv_data,
				 STRING_LD(encrypted));

  lsh_string_free(key);
  lsh_string_free(salt);
  lsh_string_free(iv);
  lsh_string_free(encrypted);

  return value;
}

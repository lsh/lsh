/* spki.h
 *
 * An implementation of SPKI certificate checking
 *
 */

/* lsh, an implementation of the ssh protocol
 *
 * Copyright (C) 2002, Niels Möller
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301  USA
 */

#ifndef LSH_SPKI_H_INCLUDED
#define LSH_SPKI_H_INCLUDED

#include <time.h>

#include "alist.h"
#include "crypto.h"

#include "spki/certificate.h"

struct lsh_string *
make_ssh_hostkey_tag(const char *host);

const struct lsh_object *
spki_algorithm_lookup(struct alist *algorithms,
		      struct sexp_iterator *i,
		      int *type);

enum lsh_atom
spki_key_type_lookup(struct sexp_iterator *i);

/* Analogous to Nettle's rsa_keypair_from_sexp_alist and rsa_keypair_to_sexp. */
int
ed25519_keypair_from_sexp_alist(const uint8_t **pub, const uint8_t **priv,
				struct sexp_iterator *i);

int
ed25519_keypair_to_sexp(struct nettle_buffer *buffer,
			const uint8_t *public, const uint8_t *private);

struct lsh_string *
spki_to_ssh_pubkey(struct sexp_iterator *i,
		   enum lsh_atom *type);

struct signer *
spki_make_signer(const struct lsh_string *key,
		 enum lsh_atom *type);

struct lsh_string *
spki_hash_data(const struct nettle_hash *algorithm,
	       int algorithm_name,
	       uint32_t length, uint8_t *data);

int
spki_authorize(const struct spki_acl_db *db,
	       const struct spki_principal *subject,
	       time_t t,
	       /* A tag expression */
	       const struct lsh_string *access);

struct spki_context *
make_spki_context(void);

int
spki_add_acls(struct spki_acl_db *db,
	      uint32_t length, const uint8_t *data);


struct lsh_string *
spki_pkcs5_encrypt(struct lsh_string *label,
		   uint32_t prf_name,
		   struct mac_algorithm *prf,
		   int crypto_name,
		   struct crypto_algorithm *crypto,
		   uint32_t salt_length,
		   struct lsh_string *password,
		   uint32_t iterations,
                   const struct lsh_string *data);

struct lsh_string *
spki_pkcs5_decrypt(struct alist *mac_algorithms,
                   struct alist *crypto_algorithms,
                   struct lsh_string *expr);

#endif /* LSH_SPKI_H_INCLUDED */
